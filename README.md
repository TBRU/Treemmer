# Treemmer


A tool to reduce large phylogenetic datasets with minimal loss of diversity.

Here you find the software, the data and scripts used in the manuscript (Influenza_data, MTB_data and stochastic_component), and a short tutorial on fine tuning the behaviour of Treemmer with the pruning options.

If you use Treemmer please cite:

Menardo et al. (2018). Treemmer: a tool to reduce large phylogenetic datasets with minimal loss of diversity. BMC Bioinformatics 19:164.

 https://doi.org/10.1186/s12859-018-2164-8.

The original Treemmer is not compatible with joblib 0.12 or greater, find a compatible version on github. 

Treemmer moved to GitHub and this page is not updated: please find the last version of Treemmer here : https://github.com/fmenardo/Treemmer 





